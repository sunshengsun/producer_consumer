﻿using System;
using System.Threading;
using System.Collections.Generic;

/*
 * 生产者-消费者 
 *
 * 一个容器，n各生产者，m各消费者
 * 生产者往容器内存放产品
 * 消费者从容器内消费产品
 *
 * 当生产者 将容器存满时，通知消费者来消费
 * 当消费者将容器搬空时，通知生产者来生产
 */
namespace producer_consumer
{
    class Program
    {
        private static int MAX_BUFF = 10; // 容器最大容量
        private static Queue<string> qu_docker = new Queue<string>(MAX_BUFF); // 仓库容器
        private static ManualResetEvent m_p = new ManualResetEvent(false); // 生产者信号
        private static ManualResetEvent m_c = new ManualResetEvent(true);  // 消费者信号
        private static object obj = new object();

        static void Producer(object pr_id)
        {
            while (true)
            {
                // 容器作为共享资源，对其的操作要加锁
                m_p.WaitOne();
                lock (obj)
                {
                    if (qu_docker.Count == MAX_BUFF)
                    {
                        Console.WriteLine("{0} 号生产者 -》 仓库满了，通知生产", ((int)pr_id).ToString());
                        m_p.Reset(); // 置生产者信号为false
                        m_c.Set(); // 通知全部消费者来消费， 这通过置消费者信号为true实现
                    }
                    else
                    {
                        Console.WriteLine("{0} 生产者 -》 生产产品啦", ((int)pr_id).ToString());
                        qu_docker.Enqueue(DateTime.Now.ToString("hh:mm:ss:fff"));
                    }
                }
                Thread.Sleep(5000);
            }
        }

        static void Consumer(object con_id)
        {
            while (true)
            {
                m_c.WaitOne();
                lock (obj)
                {
                    if (qu_docker.Count == 0)
                    {
                        Console.WriteLine("{0} 消费者 -》 仓库空了，消费者别消费了，然后通知生产者来生产", ((int)con_id).ToString());
                        m_c.Reset();
                        m_p.Set();
                    }
                    else
                    {
                        Console.WriteLine("{0} 消费者消费啦 {1}", ((int)con_id).ToString(), qu_docker.Dequeue());
                    }
                }
                Thread.Sleep(1000);
            }
        }
        
        static void Main(string[] args)
        {
            Thread[] pp = new Thread[10];

            for (int i = 0; i < 5; i++)
            {
                pp[i] = new Thread(new ParameterizedThreadStart(Producer));
                pp[i].Start(i+1);
            }

            Thread cc1 = new Thread(new ParameterizedThreadStart(Consumer));
            cc1.Start(1);

            Thread cc2 = new Thread(new ParameterizedThreadStart(Consumer));
            cc2.Start(2);

            pp[0].Join();
            pp[1].Join();
            pp[2].Join();
            pp[3].Join();
            pp[4].Join();
            cc1.Join();
            cc2.Join();
            
            
            Console.WriteLine("Hello World!");
        }
    }
}